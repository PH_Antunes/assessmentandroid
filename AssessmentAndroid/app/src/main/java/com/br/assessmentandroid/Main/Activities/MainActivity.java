package com.br.assessmentandroid.Main.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.drawable.DrawableWrapper;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.br.assessmentandroid.Main.Adapter.ListAdapter;
import com.br.assessmentandroid.Main.Interface.DataService;
import com.br.assessmentandroid.Main.Model.TaskData;
import com.br.assessmentandroid.Main.Model.TaskList;
import com.br.assessmentandroid.Main.Retrofit.RetrofitInit;
import com.br.assessmentandroid.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private ArrayList<String> listItens = new ArrayList<>();
    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listID);
        relativeLayout = (RelativeLayout) findViewById(R.id.rLayout);

        Call<TaskList> call = new RetrofitInit().getDataService().receive();

        call.enqueue(new Callback<TaskList>() {
            @Override
            public void onResponse(Call<TaskList> call, Response<TaskList> response) {
                TaskList taskList = response.body();

                if(response.isSuccessful()){
                    for(TaskData data : taskList.getTaskList()){
                        listItens.add(data.getDescription());
                    }

                    ListAdapter adapter = new ListAdapter(getApplicationContext(), listItens);

                    if(listItens.size() == 0)
                        relativeLayout.setBackgroundResource(R.drawable.empty_list);

                    listView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<TaskList> call, Throwable t) {

                if(listItens.size() == 0)
                    relativeLayout.setBackgroundResource(R.drawable.empty_list);

                Toast.makeText(getApplicationContext(),getString(R.string.request_error),Toast.LENGTH_LONG).show();

                Log.e("Erro", t.getMessage());
            }
        });

    }
}
