package com.br.assessmentandroid.Main.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.br.assessmentandroid.Main.Model.TaskData;
import com.br.assessmentandroid.R;

import java.util.ArrayList;

/**
 * Created by rsouza on 19/10/2017.
 */

public class ListAdapter extends ArrayAdapter<String> {

    private ArrayList<String> taskList;
    private Context context;

    public ListAdapter(@NonNull Context context, @NonNull ArrayList<String> objects) {
        super(context, 0, objects);

        this.taskList = objects;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = null;

        if(taskList != null){

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.list_item, parent, false);

            TextView description = (TextView) view.findViewById(R.id.list_item_text);

            String data = taskList.get(position);

            description.setText(data);
        }

        return view;
    }
}
