package com.br.assessmentandroid.Main.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lcnitrox on 16/10/17.
 */

public class TaskList {

    @SerializedName("tarefa")
    private List<TaskData> taskList;

    public List<TaskData> getTaskList(){
        return taskList;
    }
}

