package com.br.assessmentandroid.Main.Activities;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.br.assessmentandroid.Main.Model.User;
import com.br.assessmentandroid.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistrationActivity extends AppCompatActivity {


    private EditText etName;
    private EditText etLogin;
    private EditText etPassword;
    private EditText etConfirmPassword;
    private EditText etCpf;

    private Button btnSave;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        etName = (EditText) findViewById(R.id.etName);
        etLogin = (EditText) findViewById(R.id.etLogin);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        etCpf = (EditText) findViewById(R.id.etCpf);

        btnSave = (Button) findViewById(R.id.btnSave);

        firebaseAuth = FirebaseAuth.getInstance();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String password = etPassword.getText().toString();
                String login = etLogin.getText().toString();
                String cpf = etCpf.getText().toString();

                if(checkPassword()){

                    firebaseAuth.createUserWithEmailAndPassword(login, password)
                            .addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if(task.isSuccessful()){
                                        firebaseAuth.signOut();

                                        String cpf = etCpf.getText().toString();

                                        reference.child("UserData").child(cpf).setValue(buildUser());

                                        Toast.makeText(getApplicationContext(),getString(R.string.creation_success),Toast.LENGTH_LONG).show();

                                        startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
                                    }
                                    else
                                        Toast.makeText(getApplicationContext(),getString(R.string.creation_error), Toast.LENGTH_LONG).show();

                                }
                            });
                }
                else
                    Toast.makeText(getApplicationContext(),getString(R.string.password_mismatch), Toast.LENGTH_LONG).show();




            }
        });
    }

    private User buildUser(){

        User user = new User();

        user.setName(etName.getText().toString());
        user.setEmail(etLogin.getText().toString());
        user.setCpf(etCpf.getText().toString());

        return user;
    }

    private boolean checkPassword(){

        return etPassword.getText().toString().equals(etConfirmPassword.getText().toString());

    }
}
