package com.br.assessmentandroid.Main.Retrofit;

import com.br.assessmentandroid.Main.Interface.DataService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by antun on 15/10/2017.
 */

public class RetrofitInit {

    private Retrofit retrofit;

    public RetrofitInit(){

       retrofit = new Retrofit.Builder().baseUrl("http://infnet.educacao.ws/")
                .addConverterFactory(GsonConverterFactory.create()).build();
    }

    public DataService getDataService(){
        return retrofit.create(DataService.class);
    }
}
