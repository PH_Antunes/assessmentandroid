package com.br.assessmentandroid.Main.Model;

/**
 * Created by rsouza on 09/10/2017.
 */

public class User {

    private String name;
    private String email;
    private String cpf;

    public User(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
