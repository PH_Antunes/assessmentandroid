package com.br.assessmentandroid.Main.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lcnitrox on 16/10/17.
 */

public class TaskData implements Serializable {
    @SerializedName("id")
    private String id;

    @SerializedName("descricao")
    private String description;

    @SerializedName("imagem")
    private String image;

    public TaskData(String id, String description, String image) {
        this.id = id;
        this.description = description;
        this.image = image;
    }

    public TaskData(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
