package com.br.assessmentandroid.Main.Interface;

import com.br.assessmentandroid.Main.Model.TaskData;
import com.br.assessmentandroid.Main.Model.TaskList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by antun on 15/10/2017.
 */

public interface DataService {

    @GET("dadosAtividades.php")
    Call<TaskList> receive();
}
